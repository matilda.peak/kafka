# ----------------------------------------------------------------------------
# Copyright (C) 2019 Matilda Peak - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# ----------------------------------------------------------------------------

FROM registry.gitlab.com/matilda.peak/runtime-java:stable

# Labels
LABEL maintainer='Matilda Peak Limited <info@matildapeak.com>'

ENV KAFKA_USER=kafka \
    KAFKA_HOME=/opt/kafka \
    PATH=$PATH:/opt/kafka/bin

USER root

COPY fix-permissions /usr/local/bin

# Download and install kafka
ARG KAFKA_DIST=kafka_2.12-1.0.2
RUN set -x && \
    curl -fsSLO "http://www.apache.org/dist/kafka/1.0.2/$KAFKA_DIST.tgz" && \
    curl -fsSLO "http://www.apache.org/dist/kafka/1.0.2/$KAFKA_DIST.tgz.asc" && \
    curl -fsSLO "http://kafka.apache.org/KEYS" && \
    export GNUPGHOME="$(mktemp -d)" && \
    gpg --import KEYS && \
    gpg --batch --verify "$KAFKA_DIST.tgz.asc" "$KAFKA_DIST.tgz" && \
    tar -xzf "$KAFKA_DIST.tgz" -C /opt && \
    mv /opt/$KAFKA_DIST $KAFKA_HOME && \
    rm -r "$GNUPGHOME" "$KAFKA_DIST.tgz" "$KAFKA_DIST.tgz.asc"

# Copy management and monitor scripts
COPY kafka-check.sh \
     docker-entrypoint.sh \
     /opt/kafka/

# Logging configuration
COPY log4j.properties $KAFKA_HOME/config/

# Final home directory setup
RUN set -x && \
    useradd --uid 6000 --system --shell /bin/bash --user-group --home-dir $KAFKA_HOME $KAFKA_USER && \
    chown -R "$KAFKA_USER:0" $KAFKA_HOME && \
    /usr/local/bin/fix-permissions $KAFKA_HOME

WORKDIR "$KAFKA_HOME"

USER kafka

ENTRYPOINT ["./docker-entrypoint.sh"]
