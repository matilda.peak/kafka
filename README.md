---
Title:      MatildaPeak kafka docker images
Author:     Alan Christie
Date:       28 January 2018
---

[![build status](https://gitlab.com/matilda.peak/kafka/badges/master/build.svg)](https://gitlab.com/matilda.peak/kafka/commits/master)

# A docker image for Apache Kafka
The docker file is based upon the one provided by [Kubernetes Kafka contrib].

# Todo
Update to the latest version of Kafka
---

[Kubernetes Kafka contrib]: https://github.com/kubernetes/contrib/tree/master/statefulsets/kafka
